package com.example.psychotestexamined

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.Snackbar.make
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

class MainActivity : AppCompatActivity() {

    private var lobbies: ArrayList<String> = ArrayList()
    private lateinit var broadRec: BroadcastReceiver

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseApp.initializeApp(applicationContext)

        val lobbyCode = findViewById<EditText>(R.id.editLobbyCode)
        val connectBtn = findViewById<Button>(R.id.connectBtn)

        val ref = FirebaseDatabase.getInstance().getReference("lobbies")

        ref.addValueEventListener(object: ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                for(lobby in p0.children){
                    lobbies.add(lobby.key.toString())
                }
            }

        })

        var lobbyPath: String

        connectBtn.setOnClickListener{
            if(lobbyCode.text.toString() == ""){
                Toast.makeText(applicationContext, "El código de lobby no puede ser vacío", Toast.LENGTH_SHORT).show()
            }
            else{
                if(lobbies.contains(lobbyCode.text.toString())){
                    lobbyPath = lobbyCode.text.toString()
                    val intent = Intent (applicationContext, AppliedTestActivity::class.java)
                    intent.putExtra(LOBBY_CODE, lobbyPath)
                    startActivity(intent)
                    this.finish()
                }
                else{
                    Toast.makeText(applicationContext, "El lobby no existe", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {

                if(!isNetworkAvailable())
                {
                    val snackbar = make(findViewById(R.id.applyLobbyMain),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }
                    snackbar.show()
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
    companion object {
        const val LOBBY_CODE = "com.example.psychotestexamined.LOBBY_CODE"
    }
}
