package com.example.psychotestexamined

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.animation.AnimationUtils
import android.widget.*
import com.google.firebase.database.*


class AppliedTestActivity : AppCompatActivity() {

    private var idLobby: String = ""
    private var questionIndex = -1
    private lateinit var broadRec: BroadcastReceiver
    private var resultadoEscogido = false
    private var resultadoScore = "null"
    private lateinit var btnSiguiente: Button
    private var respondeExaminado = false
    private var acabo = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_applied_test)

        idLobby = intent.getStringExtra(MainActivity.LOBBY_CODE)
        btnSiguiente = findViewById(R.id.btnSiguiente)

        btnSiguiente.setOnClickListener {
            siguienteItem()
            btnSiguiente.isEnabled = false
        }

        val lobbyCode = findViewById<TextView>(R.id.lobbyIDLabel)
        lobbyCode.text = lobbyCode.text.toString() + " " +idLobby

        lobbyCode.setOnClickListener {
            val builder = AlertDialog.Builder(this)

            // Set the alert dialog title
            builder.setTitle("Lobby " + idLobby)

            // Display a message on alert dialog
            builder.setMessage("¿Volver al menu principal?")

            // Set a positive button and its click listener on alert dialog
            builder.setPositiveButton("Si"){dialog, which ->
                // Do something when user press the positive button
                val intent = Intent (applicationContext, MainActivity::class.java)
                startActivity(intent)
                this.finish()
            }

            // Display a neutral button on alert dialog
            builder.setNeutralButton("Cancelar"){_,_ ->

            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }

        val questionTxt = findViewById<TextView>(R.id.questionText)

        val ref = FirebaseDatabase.getInstance().getReference("lobbies").child(idLobby).child("items")

        val contexto = this

        ref.addChildEventListener(object: ChildEventListener{
            override fun onChildMoved(p0: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {

                val resItemId = p0.child("resItemId").value.toString()
                //Toast.makeText(applicationContext, resItemId, Toast.LENGTH_LONG).show()
                if(resItemId != "null")
                {
                    val rutaBD = p0.child("rutaBD").value.toString()
                    val ref2 = FirebaseDatabase.getInstance().getReference("$rutaBD/Bloques")

                    val nuevoResultado = ResultadoItem(resItemId,
                        p0.child("idTest").value.toString(), p0.child("idSubTest").value.toString(),
                        p0.child("idItem").value.toString(), p0.child("resultadoTool").value.toString(),
                        resultadoScore)

                    ref2.child(resItemId).setValue(nuevoResultado)

                }

            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                acabo = item.child("esUltimo").value.toString().toBoolean()
                questionIndex++
                //Toast.makeText(applicationContext, "Evento", Toast.LENGTH_LONG).show()
                questionTransitionAnimationOut(questionTxt)
                questionTransitionAnimationIn(questionTxt)

                questionTxt.text = item.child("enunciado").value.toString()

                val parentLinearLayout = findViewById<LinearLayout>(R.id.parent_radio_group)
                parentLinearLayout.removeAllViews()
                respondeExaminado = item.child("respondeExaminado").value.toString().toBoolean()
                resultadoEscogido = false

                if(respondeExaminado)
                {
                    btnSiguiente.isEnabled = true
                    val inflater: LayoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val view = inflater.inflate(R.layout.radio_group_layout, null)

                    parentLinearLayout.addView(view, parentLinearLayout.childCount - 1)

                    val layout = findViewById<LinearLayout>(R.id.RadioGroupLayout)
                    val radioGroupScore = RadioGroup(contexto)

                    radioGroupScore.orientation = RadioGroup.VERTICAL

                    var radioBtn: RadioButton
                    var radioParams: RadioGroup.LayoutParams

                    if(item.child("tipoScore").value.toString() == "Textual")
                    {
                        for(opcion in item.child("configuracionScore").children)
                        {
                            radioBtn = RadioButton(contexto)
                            radioBtn.text = opcion.value.toString()

                            radioParams =
                                RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.MATCH_PARENT)

                            radioGroupScore.addView(radioBtn, radioParams)
                        }

                    }
                    else
                    {
                        var i = item.child("configuracionScore").child("Inicial").value.toString().toDouble()
                        val fin = item.child("configuracionScore").child("Fin").value.toString().toDouble()
                        val salto = item.child("configuracionScore").child("Salto").value.toString().toDouble()

                        while(i <= fin) {
                            radioBtn = RadioButton(contexto)
                            radioBtn.text = "$i"

                            radioParams =
                                RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.MATCH_PARENT)

                            radioGroupScore.addView(radioBtn, radioParams)

                            i += salto
                        }
                    }

                    radioGroupScore.setOnCheckedChangeListener { _, checkedId ->
                        resultadoScore = findViewById<RadioButton>(checkedId).text.toString()
                        resultadoEscogido = true

                    }

                    layout.addView(radioGroupScore)

                }
                else
                {
                    btnSiguiente.isEnabled = false
                }

            }

            override fun onChildRemoved(p0: DataSnapshot) {

            }

            override fun onCancelled(p0: DatabaseError) {

            }


        })

    }

    private fun siguienteItem()
    {
        if(respondeExaminado && !resultadoEscogido)
        {
            Toast.makeText(applicationContext, "Debe seleccionar una opción", Toast.LENGTH_LONG).show()
            return
        }

        val ref = FirebaseDatabase.getInstance().getReference("lobbies").child(idLobby).child("items").child("it$questionIndex")

        ref.child("estado").setValue(resultadoScore)
        if(acabo) {
            findViewById<TextView>(R.id.questionText).text = "Test terminado. Gracias."
            val parentLinearLayout = findViewById<LinearLayout>(R.id.parent_radio_group)
            parentLinearLayout.removeAllViews()
        }
    }

    fun questionTransitionAnimationIn(questionText: TextView){
        var animation = AnimationUtils.loadAnimation(this, R.anim.abc_grow_fade_in_from_bottom)
        questionText.startAnimation(animation)
    }

    fun questionTransitionAnimationOut(questionText: TextView){
        var animation = AnimationUtils.loadAnimation(this, R.anim.abc_shrink_fade_out_from_bottom)
        questionText.startAnimation(animation)
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
        return if (connectivityManager is ConnectivityManager) {
            val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
            networkInfo?.isConnected ?: false
        } else false
    }

    override fun onStart() {
        super.onStart()

        broadRec = object : BroadcastReceiver() {

            override fun onReceive(context: Context, intent: Intent) {
                val lobbyCode = findViewById<TextView>(R.id.lobbyIDLabel)
                if(!isNetworkAvailable())
                {

                    lobbyCode.setBackgroundColor(Color.RED)
                    val snackbar = Snackbar.make(findViewById(R.id.applyActivityLobby),"Sin conexión, los datos se actualizarán una vez vuelva la conexión", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("Aceptar") { snackbar.dismiss() }

                    snackbar.show()
                }
                else {
                    lobbyCode.setBackgroundColor(Color.GREEN)
                }
            }
        }

        registerReceiver(broadRec, IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"))
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(broadRec)
    }
}
